package entities;

import lombok.*;

import java.util.Date;
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@ToString

public class Order {

    private int id;
    private int petId;
    private int quantity;
    private String shipDate;
    private String status;
    private String complete;
}
