package entities;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class DeleteResponse {
    private int code;
    private String type;
    private String message;


}
