package entities;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode


public class User {
    private int id;
    private String username;
    private String  firstName;
    private String lastName;
    private String email;
    private String password;
    private String phone;
    private int userStatus;


    @Override
    public String toString() {
        return "User: \n" +
                "id= " + id +"\n"+
                "username= " + username + "\n" +
                "firstName= " + firstName + "\n" +
                "lastName= " + lastName + "\n" +
                "email= " + email + "\n" +
                "password= " + password + "\n" +
                "phone= " + phone + "\n" +
                "userStatus= " + userStatus+"\n";
    }
}
