package entities;

import data.Statuses;
import lombok.*;

import java.util.Arrays;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode

public class PetWithStatusNonEnum {
    private long id;
    private Category category;
    private String name;
    private List<String> photoUrls;
    private List<Category> tags;
    private String status;

    @Override
    public String toString() {
        return "Pet: \n" +
                "Id = " + this.id + "\n" +
                "Category = " + this.category.getName() + "\n" +
                "Name = " + this.name + "\n" +
                "PhotoUrls = " + Arrays.toString(this.photoUrls.toArray()) + "\n" +
                "Tags = " + Arrays.toString(this.tags.stream().map(Category::getName).toArray()) + "\n" +
                "Status = " + this.status + "\n";
    }


}
