import data.Statuses;
import entities.*;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.mapper.ObjectMapper;
import io.restassured.module.jsv.JsonSchemaValidator;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.math.BigInteger;
import java.sql.Array;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;

import static io.restassured.RestAssured.basePath;
import static io.restassured.RestAssured.given;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import static org.hamcrest.Matchers.equalTo;

public class APITests {
    @Before
    public void setUp() {
        RestAssured.baseURI = "https://petstore.swagger.io/v2";
    }

    @After
    public void tearDown() {
    }
    @Test
    public void getInfoAboutDarthVader(){
        given().baseUri("https://swapi.dev/api")
                .basePath("people")
                .when()
                .get("4")
                .then()
                .statusCode(200)
                .and()
                .contentType(ContentType.JSON)
                .and()
                .log()
                .all()
                .body("name",equalTo("Darth Vader"));


    }
    @Test
    public void addPetToSore() {

        Category cats = new Category(125, "Cats");
        Category test = new Category(225, "TestCategory");

        Pet cat = new Pet(
                1000 + (long) (Math.random() * 9999),
                cats,
                "CatForTests",
                Collections.singletonList("urls"),
                Arrays.asList(cats, test),
                Statuses.AVAILABLE
        );

        Response responseAddPet = given()
                .basePath("/pet")
                .contentType(ContentType.JSON)
                .body(cat)
                .post();

        Assert.assertEquals("Wrong statusCode", 200, responseAddPet.getStatusCode());
        System.out.println("Response for adding pet" + "\n" + responseAddPet.asString() + "\n");

        Pet addedPet = responseAddPet.as(Pet.class);

        Pet foundPetById = given()
                .pathParam("Id", addedPet.getId())
                .basePath("/pet/{Id}")
                .accept("application/json")
                .when()
                .get()
                .as(Pet.class);
        System.out.println("Response for getting pet by Id: \n" + foundPetById.toString());

        Assert.assertEquals("Wrong name", addedPet.getName(), foundPetById.getName());

        Response deleteResponse = given()
                .pathParam("Id", addedPet.getId())
                .basePath("/pet/{Id}")
                .accept("application/json")
                .when()
                .delete();
        System.out.println(deleteResponse.prettyPrint());

        DeleteResponse deleteResponseAssClass = deleteResponse.as(DeleteResponse.class);

        Assert.assertEquals("Wrong StatusCode", 200, deleteResponse.getStatusCode());
        Assert.assertEquals("Wrong Message", addedPet.getId(), Long.parseLong(deleteResponseAssClass.getMessage()));
    }

        @Test
        public void validationJSONSchemaOfResponseGetPet(){

            Category cats = new Category(125, "Cats");
            Category test = new Category(225, "TestCategory");

            Pet cat = new Pet(
                    1000 + (long) (Math.random() * 9999),
                    cats,
                    "CatForTests",
                    Collections.singletonList("urls"),
                    Arrays.asList(cats, test),
                    Statuses.AVAILABLE
            );

            Pet responseAddPet = given()
                    .basePath("/pet")
                    .contentType(ContentType.JSON)
                    .body(cat)
                    .post()
                    .as(Pet.class);

            given()
                    .basePath("/pet/" + responseAddPet.getId())
                    .accept("application/json")
                    .get()
                    .then()
                    .assertThat()
                    .body(matchesJsonSchemaInClasspath("petSchema.json"));
    }

    @Test
    public  void createPetWithInvalidId(){
        Category cats = new Category(125, "Cats");
        Category test = new Category(225, "TestCategory");

        BigInteger petId=new BigInteger("98745632112345678965");

        PetWithHugeId cat = new PetWithHugeId(
                petId,
                cats,
                "CatForTests",
                Collections.singletonList("urls"),
                Arrays.asList(cats, test),
                Statuses.AVAILABLE
        );

        Response responseAddNewPet = given()
                .basePath("/pet")
                .contentType(ContentType.JSON)
                .body(cat)
                .post();

        Assert.assertEquals("Wrong StatusCode",500,responseAddNewPet.getStatusCode());
        System.out.println("Response for adding pet" + "\n" + responseAddNewPet.asString() + "\n");
    }

    @Test
    public void changePetInformation(){
        Category parrots = new Category(325, "Parrots");
        Category test = new Category(425, "TestCategory");

        Pet parrot = new Pet(
                1000 + (long) (Math.random() * 9999),
                parrots,
                "Cockatoo",
                Collections.singletonList("urls"),
                Arrays.asList(parrots, test),
                Statuses.AVAILABLE
        );

        Response responseAddPet = given()
                .basePath("/pet")
                .contentType(ContentType.JSON)
                .body(parrot)
                .post();
        Assert.assertEquals("Wrong StatusCode",200,responseAddPet.getStatusCode());
        System.out.println("Response for adding pet" + "\n" + responseAddPet.asString() + "\n");


        Pet changedParrot=new Pet(
                parrot.getId(),
                parrot.getCategory(),
                "Ara",
                parrot.getPhotoUrls(),
                parrot.getTags(),
                Statuses.SOLD
        );

        Response responseChangePet=given()
                .basePath("/pet")
                .contentType(ContentType.JSON)
                .body(changedParrot)
                .when()
                .put();

        Assert.assertEquals("Wrong StatusCode",200,responseChangePet.getStatusCode());
        System.out.println("Response for changing pet" + "\n" + responseChangePet.asString() + "\n");

        Pet addedPet = responseAddPet.as(Pet.class);
        Pet changedPet=responseChangePet.as(Pet.class);

        Pet foundPetById = given()
                .pathParam("Id", addedPet.getId())
                .basePath("/pet/{Id}")
                .accept("application/json")
                .when()
                .get()
                .as(Pet.class);
        System.out.println("Response for getting pet by Id: \n" + foundPetById.toString());

        Assert.assertEquals("Wrong Name",changedPet.getName(),foundPetById.getName());
        Assert.assertEquals("Wrong Status",changedPet.getStatus(),foundPetById.getStatus());
    }

    @Test
    public void validationJSONSchemaOfResponseGetUser() {


        User user = new User(
                1 + (int) (Math.random() * 999),
                "User" + RandomStringUtils.randomAlphabetic(5),
                "First Name" + RandomStringUtils.randomAlphabetic(5),
                "Last Name" + RandomStringUtils.randomAlphabetic(5),
                "user@" + RandomStringUtils.randomAlphabetic(5) + ".com",
                RandomStringUtils.randomAlphabetic(8),
                "8" + (int)(Math.random() * 999999999),
                111
        );
        System.out.println(user.toString());
        Response responseAddUser=given()
                .basePath("/user")
                .contentType(ContentType.JSON)
                .body(user)
                .post();
        System.out.println(responseAddUser.prettyPrint());

        Assert.assertEquals("Wrong StatusCode",200,responseAddUser.getStatusCode());

        given()
                .basePath("/pet/" + user.getUsername())
                .accept("application/json")
                .get()
                .then()
                .assertThat()
                .body(matchesJsonSchemaInClasspath("userSchema.json"));

    }

    @Test
    public void deletePet(){
        Category cats = new Category(125, "Cats");
        Category test = new Category(225, "TestCategory");

        Pet cat = new Pet(
                1000 + (long) (Math.random() * 9999),
                cats,
                "CatForTests",
                Collections.singletonList("urls"),
                Arrays.asList(cats, test),
                Statuses.AVAILABLE
        );

        Response responseAddPet = given()
                .basePath("/pet")
                .contentType(ContentType.JSON)
                .body(cat)
                .post();

        Assert.assertEquals("Wrong statusCode", 200, responseAddPet.getStatusCode());
        System.out.println("Response for adding pet" + "\n" + responseAddPet.asString() + "\n");

        Pet addedPet=responseAddPet.as(Pet.class);

        Response deleteResponse = given()
                .pathParam("Id", addedPet.getId())
                .basePath("/pet/{Id}")
                .accept("application/json")
                .when()
                .delete();
        System.out.println(deleteResponse.prettyPrint());

        DeleteResponse deleteResponseAssClass = deleteResponse.as(DeleteResponse.class);

        Assert.assertEquals("Wrong StatusCode", 200, deleteResponse.getStatusCode());

        given()
                .pathParam("Id", addedPet.getId())
                .basePath("/pet/{Id}")
                .accept("application/json")
                .when()
                .get()
                .then()
                .body("message",equalTo("Pet not found"))
                .and()
                .log()
                .body();
    }

    @Test
    public void createPetWithSoldStatus(){
        Category cats = new Category(125, "Cats");
        Category test = new Category(225, "TestCategory");

        PetWithStatusNonEnum cat = new PetWithStatusNonEnum(
                1000 + (long) (Math.random() * 9999),
                cats,
                "CatForTests",
                Collections.singletonList("urls"),
                Arrays.asList(cats, test),
                "sold"
        );

        Response responseAddPet = given()
                .basePath("/pet")
                .contentType(ContentType.JSON)
                .body(cat)
                .post();

        Assert.assertEquals("Wrong statusCode", 200, responseAddPet.getStatusCode());
        System.out.println("Response for adding pet" + "\n" + responseAddPet.asString() + "\n");

        PetWithStatusNonEnum addedPet=responseAddPet.as(PetWithStatusNonEnum.class);

        Response foundPetById = given()
                .pathParam("Id", addedPet.getId())
                .basePath("/pet/{Id}")
                .accept("application/json")
                .when()
                .get();

        System.out.println("Response for getting pet by Id: \n" + foundPetById.asString());

        PetWithStatusNonEnum foundPet=foundPetById.as(PetWithStatusNonEnum.class);

        Assert.assertEquals("Wrong Status",addedPet.getStatus(),foundPet.getStatus());




        PetWithStatusNonEnum [] getPetByStatus= given()
                .basePath("/pet/findByStatus?status=sold")
                .accept("application/json")
                .when()
                .get()
                .as(PetWithStatusNonEnum[].class);

        for (int i = 0; i < getPetByStatus.length ; i++) {
            if (getPetByStatus[i].getId() == addedPet.getId()) {
                Assert.assertEquals("Wrong name",getPetByStatus[i].getName(), addedPet.getName());
            }

            }

    }
    }















